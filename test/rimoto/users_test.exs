defmodule Rimoto.UsersTest do
  use Rimoto.DataCase, async: true

  alias Rimoto.{Users, Users.User}

  describe "list_users_with_higher_number/2" do
    test "returns an empty list if no users are present" do
      assert Users.list_users_with_higher_number(5) == []
    end

    test "returns all users with a points value greater or equal than the given value" do
      insert_users([2, 5, 28])

      [first_user, second_user] =
        Users.list_users_with_higher_number(5) |> Enum.sort(&(&1.points <= &2.points))

      assert first_user.points == 5
      assert second_user.points == 28
    end

    test "returns no users if the given max_value is higher than any user points" do
      insert_users([10, 87, 28])

      assert [] = Users.list_users_with_higher_number(88)
    end

    test "returns a limited amount of users with a points value greater or equal than the given value" do
      insert_users([2, 5, 28, 45])

      [single_user] =
        Users.list_users_with_higher_number(3, limit: 1)
        |> Enum.sort(&(&1.points <= &2.points))

      assert single_user.points in [5, 28, 45]
    end
  end

  def insert_users(points_list) do
    user_count = length(points_list)
    now = DateTime.utc_now()

    users_attrs =
      Enum.map(points_list, fn points -> %{points: points, inserted_at: now, updated_at: now} end)

    {^user_count, nil} = Rimoto.Repo.insert_all(User, users_attrs)
  end
end
