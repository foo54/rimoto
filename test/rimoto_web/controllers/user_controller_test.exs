defmodule RimotoWeb.UserControllerTest do
  use RimotoWeb.ConnCase

  import Ecto.Query

  alias Rimoto.Users.User

  setup do
    start_supervised!({Rimoto.Pointer, schedule_refresh_on_start: false, max_number: 5})

    :ok
  end

  describe "GET /" do
    test "returns an empty response if no users exist", %{conn: conn} do
      conn = get(conn, "/")
      assert json_response(conn, 200) == %{"timestamp" => nil, "users" => []}
    end

    test "returns two users with points > max_number if users exist", %{
      conn: conn
    } do
      now = DateTime.utc_now()

      # could become a generic insert_all helper or maybe use ExMachina instead?
      user_attrs =
        Enum.map(
          [%{points: 2}, %{points: 10}, %{points: 28}],
          fn attrs -> Map.merge(attrs, %{inserted_at: now, updated_at: now}) end
        )

      {3, nil} = Rimoto.Repo.insert_all(Rimoto.Users.User, user_attrs)

      [_user_1, %{id: user_2_id}, %{id: user_3_id}] =
        from(u in User, order_by: [asc: :points]) |> Rimoto.Repo.all()

      conn = get(conn, "/")
      assert %{"timestamp" => nil, "users" => users} = json_response(conn, 200)

      assert Enum.sort(users, &(&1["points"] <= &2["points"])) == [
               %{"id" => user_2_id, "points" => 10},
               %{"id" => user_3_id, "points" => 28}
             ]

      # the timestamp should not be a datetime on subsequent requests
      conn = get(conn, "/")

      assert %{"timestamp" => hopefully_updated_timestamp, "users" => users} =
               json_response(conn, 200)

      assert match?({:ok, %DateTime{}, _}, DateTime.from_iso8601(hopefully_updated_timestamp))

      assert Enum.sort(users, &(&1["points"] <= &2["points"])) == [
               %{"id" => user_2_id, "points" => 10},
               %{"id" => user_3_id, "points" => 28}
             ]
    end
  end
end
