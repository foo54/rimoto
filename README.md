# Rimoto

To start the application:

- Install dependencies with `mix deps.get`
- Create and migrate your database with `mix ecto.setup`
- Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

There is only one endpoint available, at [`localhost:4000`](http://localhost:4000).

## Implementation

### Updating the points of all users

`Rimoto.Users.update_all_user_points/0`

I originally used a "chunked stream" approach - i.e. get batches of rows from the database that I
would then turn into batch updates - which I thought would be easier on the database itself...
until it was pointed out during the discussion that the stream was wrapped in a single transaction,
thus most likely cancelling any potential benefits.

I have since then reverted to a straightforward "update all" rows approach which is actually
faster but it should probably be tested whether or not it puts any kind of unwanted strain on the
database with higher orders of magnitude.

### GenServer

`Rimoto.Users.Pointer`

I quickly realized that having the GenServer be the one to supervise the point update would
result in it not being able to handle any other calls - impacting directly the response time of the
main endpoint.

To avoid such an issue, I decided to make the points update an async task that is handled by a task
supervisor, purely to avoid the update being potentially being brought down by the genserver were
it to stop/crash.

## Tests

I've unfortunately been suffering from a lack of time (and sleep...) over the last months as I
have a four-months old baby at home. Spending more time on the coding challenge would have meant
sacrificing precious time on my side.

As a result, this code is _not_ production-ready in my eyes as it is lacking some crucial tests. I
have however decided to at least add a simple endpoint test to show that I do know how to write
(at least one!) test.

I may add more tests later on, especially given how I failed to explain properly why I did not add
more tests during the discussion.

This README update may or may not influence the ultimate decision but at least I've learnt my
lesson!
