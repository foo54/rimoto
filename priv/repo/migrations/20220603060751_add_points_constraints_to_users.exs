defmodule Rimoto.Repo.Migrations.AddPointsConstraintsToUsers do
  use Ecto.Migration

  def change do
    execute("ALTER TABLE users ADD CHECK (points BETWEEN 0 AND 100)")
  end
end
