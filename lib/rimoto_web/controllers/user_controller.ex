defmodule RimotoWeb.UserController do
  use RimotoWeb, :controller

  @spec retrieve_users(Connection.t(), map()) :: Connection.t()
  def retrieve_users(conn, _params) do
    %{users: users, timestamp: timestamp} = Rimoto.Pointer.retrieve_users()

    users_response = Enum.map(users, fn user -> %{id: user.id, points: user.points} end)
    timestamp = if timestamp, do: DateTime.to_iso8601(timestamp), else: nil

    conn
    |> put_status(:ok)
    |> json(%{users: users_response, timestamp: timestamp})
  end
end
