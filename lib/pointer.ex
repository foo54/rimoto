defmodule Rimoto.Pointer do
  use GenServer

  require Logger

  alias Rimoto.Users

  @name __MODULE__

  # CLIENT

  def retrieve_users(), do: GenServer.call(@name, :retrieve_users)

  # SERVER

  @refresh_timer :timer.minutes(1)
  @user_amount 2

  def start_link(init_args) do
    GenServer.start_link(__MODULE__, init_args, name: @name)
  end

  @impl true
  def init(init_args) do
    if Keyword.get(init_args, :schedule_refresh_on_start, true) == true do
      schedule_points_refresh()
    end

    initial_max_number = Keyword.get(init_args, :max_number, Users.random_number())
    Logger.info("Starting Genserver #{@name} with max_number '#{initial_max_number}'")

    {:ok, %{max_number: initial_max_number, last_query_time: nil, task_ref: nil}}
  end

  @impl true
  def handle_info(:refresh_points, %{task_ref: ref} = state) when is_reference(ref) do
    Logger.error("Attempted to refresh points while a refresh was already in progress")
    {:noreply, state}
  end

  @impl true
  def handle_info(:refresh_points, %{task_ref: nil} = state) do
    new_max_number = Users.random_number()
    Logger.info("Refreshing user points, new max_number '#{new_max_number}'")

    task = Task.Supervisor.async_nolink(Rimoto.TaskSupervisor, &Users.update_all_user_points/0)
    schedule_points_refresh()

    {:noreply, %{state | max_number: new_max_number, task_ref: task.ref}}
  end

  def handle_info({ref, result}, %{task_ref: ref} = state) do
    Process.demonitor(ref, [:flush])

    case result do
      :ok -> Logger.info("Finished refreshing user points")
      :error -> Logger.info("Failed to refresh user points")
    end

    {:noreply, %{state | task_ref: nil}}
  end

  def handle_info({:DOWN, ref, :process, _pid, reason}, %{task_ref: ref} = state) do
    Logger.error("The refresh task failed unexpectedly - #{inspect(reason)}")
    {:noreply, %{state | task_ref: nil}}
  end

  @impl true
  def handle_call(:retrieve_users, _from, state) do
    # note - we could probably cache this but the problem seems to imply that they must be queried
    users = Rimoto.Users.list_users_with_higher_number(state.max_number, limit: @user_amount)

    {
      :reply,
      %{users: users, timestamp: state.last_query_time},
      %{state | last_query_time: DateTime.utc_now()}
    }
  end

  defp schedule_points_refresh(timer \\ @refresh_timer) do
    Logger.info("Scheduling a point refresh in #{ceil(timer / 1_000)} seconds")
    Process.send_after(self(), :refresh_points, timer)
  end
end
