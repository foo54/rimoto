defmodule Rimoto.Users do
  import Ecto.Query

  require Logger

  alias Rimoto.Users.User

  @points %{min_bound: 0, max_bound: 100}
  @default_range @points.min_bound..@points.max_bound

  @spec list_users_with_higher_number(max_points :: non_neg_integer()) :: [User.t()]
  def list_users_with_higher_number(max_points, opts \\ [])
      when max_points >= @points.min_bound and @points.max_bound <= @points.max_bound do
    limit = opts[:limit]

    from(u in User, where: u.points >= ^max_points)
    # note - not sure if this is a "good" way to do this, having a bit of "fun" with then/2!
    |> then(fn query -> if limit, do: limit(query, ^limit), else: query end)
    |> Rimoto.Repo.all()
  end

  @spec update_all_user_points() :: :ok | :error
  def update_all_user_points() do
    max_bound = @points.max_bound

    {updated_counter, _} =
      from(u in User,
        update: [
          set: [points: fragment("random() * ?", ^max_bound), updated_at: fragment("now()")]
        ]
      )
      |> Rimoto.Repo.update_all([])

    if updated_counter > 0, do: :ok, else: :error
  end

  @spec random_number(Range.t()) :: non_neg_integer()
  def random_number(range \\ @default_range), do: Enum.random(range)
end
