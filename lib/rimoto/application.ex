defmodule Rimoto.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      Rimoto.Repo,
      RimotoWeb.Telemetry,
      {Phoenix.PubSub, name: Rimoto.PubSub},
      RimotoWeb.Endpoint,
      {Task.Supervisor, name: Rimoto.TaskSupervisor}
    ]

    children =
      if Application.fetch_env!(:rimoto, :start_pointer) do
        children ++ [{Rimoto.Pointer, schedule_refresh_on_start: true}]
      else
        children
      end

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Rimoto.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    RimotoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
