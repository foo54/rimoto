defmodule Rimoto.Users.User do
  use Ecto.Schema

  @type t :: %__MODULE__{
          id: pos_integer(),
          points: non_neg_integer(),
          inserted_at: DateTime.t(),
          updated_at: DateTime.t()
        }

  # note - I decided against a Jason.Encoder mostly because it might not always satisfy all endpoint needs
  # @derive {Jason.Encoder, only: [:id, :points]}
  schema "users" do
    field(:points, :integer)

    timestamps(type: :utc_datetime_usec)
  end
end
